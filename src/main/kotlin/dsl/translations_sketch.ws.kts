package dsl

println("hello")

data class Template(
    val template: String,
) {
    private fun extractSubstrings(input: String): List<String> {
        val regex = "\\{\\{(.*?)}}".toRegex()
        return regex.findAll(input).map { it.groupValues[1] }.toList()
    }
    val parameters: List<String> = extractSubstrings(template)
}

val parameters = Template("{{salute}}, {{name}}!").parameters

println(parameters)



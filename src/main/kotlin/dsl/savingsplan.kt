package dsl

import dsl.Frequency.*
import java.math.BigDecimal
import java.time.ZonedDateTime
import java.util.*

@DslMarker
annotation class SavingsPlanServiceDslMarker

/**
 * We allow use subscribe to the multiple savings plans of same isin, freq but different days,
 * (isin, freq, day)
 * So that a biweekly savings plan ~ two monthly plan
 * a weekly plan ~ four monthly plans
 */
enum class Frequency {
    WEEKLY,
    MONTHLY,
}

class SavingsPlanSystem(val name: String) {
    /**
     * mutable list simulates inside mutable states, our storage like DB
     */
    private val _savingsPlans = mutableListOf<SavingsPlan>()
    private val _subscriptions = mutableListOf<Subscription>()
    private val _schedules = mutableListOf<ExecutionSchedule>()

    val savingsPlans
        get() = _savingsPlans.toList()
    val subscriptions
        get() = _subscriptions.toList()

    val schedules
        get() = _schedules.toList()

    /**
     * functions are API the system provides
     */
    fun addSavingsPlan(savingsPlan: SavingsPlan) {
        _savingsPlans.add(savingsPlan)
    }

    fun addSubscription(subscription: Subscription) {
        _subscriptions.add(subscription)
    }

    fun addExecutionSchedule(schedule: ExecutionSchedule) {
        _schedules.add(schedule)
    }

    companion object {

        fun nextExecutionDatetime(now: ZonedDateTime, schedule: ExecutionSchedule): ZonedDateTime =
            when (schedule.frequency) {
                WEEKLY -> {
                    val diff = now.dayOfWeek.value - schedule.dayOfInterval
                    if (diff > 0) now.plusWeeks(1L).minusDays(diff.toLong())
                    else now.plusDays(diff.toLong())
                }
                MONTHLY -> {
                    val diff = now.dayOfMonth - schedule.dayOfInterval
                    if (diff > 0) now.plusMonths(1L).minusDays(diff.toLong())
                    else now.plusDays(diff.toLong())
                }

            }
    }
}

interface SavingsPlanScheduler {
    fun nextSavingsPlan(schedules: Collection<ExecutionSchedule>, plans: Collection<SavingsPlan>): SavingsPlan?
}

class PeriodicPollSavingsPlanScheduler : SavingsPlanScheduler {

    override fun nextSavingsPlan(
        schedules: Collection<ExecutionSchedule>,
        plans: Collection<SavingsPlan>
    ): SavingsPlan? {
        val dueSchedules = schedulesDue(ZonedDateTime.now(), schedules)
        return savingsPlanDue(dueSchedules, plans)
    }

    private fun schedulesDue(
        now: ZonedDateTime,
        schedules: Collection<ExecutionSchedule>
    ): Collection<ExecutionSchedule> =
        schedules.filter {
            (it.frequency == WEEKLY && now.dayOfWeek.value == it.dayOfInterval) ||
                    (it.frequency == MONTHLY && it.dayOfInterval == now.dayOfMonth)
        }

    private fun savingsPlanDue(schedules: Collection<ExecutionSchedule>, plans: Collection<SavingsPlan>) =
        plans.firstOrNull {
            it.schedule in schedules
        }
}

/**
 * data classes corresponds to our data models
 */
data class SavingsPlan(
    val id: Int,
    val instrumentId: String,
    val schedule: ExecutionSchedule,
    val nextExecution: ZonedDateTime
) // tuple(instrument, schedule)

data class ExecutionSchedule(val frequency: Frequency, val dayOfInterval: Int) // tuple

data class Amount(val value: BigDecimal, val currency: String) // Currency class in java.util?
data class Subscription(
    val savingsPlanId: Int,
    val account: Int,
    val amount: Amount
) // tuple(savings_plan, user, amount)

inline fun savingsPlanSystem(config: SavingsPlanServiceDsl.() -> Unit): SavingsPlanSystem {
    val d = SavingsPlanServiceDsl().apply(config)
    return SavingsPlanSystem(d.name).apply {
        d.savingsPlansList.forEach { this.addSavingsPlan(it) }
        d.subscriptionsList.forEach { this.addSubscription(it) }
        d.scheduleList.forEach { this.addExecutionSchedule(it) }
    }
}

@SavingsPlanServiceDslMarker
class SavingsPlanServiceDsl {

    lateinit var name: String

    private val _savingsPlans = mutableListOf<SavingsPlan>()
    val savingsPlansList
        get() = _savingsPlans.toList()

    private val _subscriptions = mutableListOf<Subscription>()
    val subscriptionsList
        get() = _subscriptions.toList()

    private val _schedules = mutableListOf<ExecutionSchedule>()
    val scheduleList
        get() = _schedules.toList()

    val savingsPlans = SavingsPlanConfigDsl()
    val subscriptions = SubscriptionConfigDsl()
    val schedules = ScheduleConfigDsl()

    @SavingsPlanServiceDslMarker
    inner class SavingsPlanConfigDsl {

        private val _savingsPlans = this@SavingsPlanServiceDsl._savingsPlans

        operator fun invoke(config: SavingsPlanConfigDsl.() -> Unit) {
            this.apply(config)
        }

        val plan
            get() = EmptySavingsPlan()

        inner class EmptySavingsPlan() {
            infix fun id(id: Int) =
                IdentifiedSP(this, id)
        }

        inner class IdentifiedSP(val previous: EmptySavingsPlan, val id: Int) {
            infix fun of(isin: String) =
                InstrumentedSP(this, isin)

        }

        inner class InstrumentedSP(val previous: IdentifiedSP, val isin: String) {
            infix fun run(frequency: Frequency) =
                FrequencySP(this, frequency)
        }

        inner class FrequencySP(private val previous: InstrumentedSP, private val frequency: Frequency) {
            infix fun on(dayOfInterval: Int) {
                val schedule = ExecutionSchedule(frequency, dayOfInterval)
                val nextExecution = SavingsPlanSystem.nextExecutionDatetime(ZonedDateTime.now(), schedule)

                _savingsPlans.add(
                    SavingsPlan(previous.previous.id, previous.isin, schedule, nextExecution)
                )
            }
        }
    }

    @SavingsPlanServiceDslMarker
    inner class ScheduleConfigDsl {
        private val _schedules = this@SavingsPlanServiceDsl._schedules
        operator fun invoke(config: ScheduleConfigDsl.() -> Unit) {
            this.apply(config)
        }

        val weekly
            get() = ScheduleWithFreq(WEEKLY)

        val monthly
            get() = ScheduleWithFreq(MONTHLY)

        inner class ScheduleWithFreq(private val frequency: Frequency) {
            infix fun day(dayOfInterval: Int) {
                _schedules.add(ExecutionSchedule(frequency, dayOfInterval))
            }
        }

    }

    @SavingsPlanServiceDslMarker
    inner class SubscriptionConfigDsl {
        operator fun invoke(config: SubscriptionConfigDsl.() -> Unit) {
            this.apply(config)
        }

        val subscribe
            get() = EmptySubscription()

        private val _subscriptions = this@SavingsPlanServiceDsl._subscriptions

        inner class EmptySubscription {
            infix fun sp(savingsPlanId: Int) = TargetedSubscription(this, savingsPlanId)
        }

        inner class TargetedSubscription(val previous: EmptySubscription, val savingsPlanId: Int) {
            infix fun by(accountNo: Int) = AccountSpecifiedSubscription(this, accountNo)
        }

        inner class AccountSpecifiedSubscription(private val previous: TargetedSubscription, private val account: Int) {
            infix fun eur(amount: String) = _subscriptions.add(
                Subscription(
                    previous.savingsPlanId,
                    account,
                    Amount(BigDecimal(amount), Currency.getInstance(Locale.GERMANY).currencyCode)
                )
            )

            infix fun usd(amount: String) = _subscriptions.add(
                Subscription(
                    previous.savingsPlanId,
                    account,
                    Amount(BigDecimal(amount), Currency.getInstance(Locale.US).currencyCode)
                )
            )
        }
    }
}

fun main() {
    val system = savingsPlanSystem {

        name = "demo"

        schedules {
            (1..7).forEach { weekly day it }
            (1..31).forEach { monthly day it }
        }

        /**
         * if a subscription is added before the next_exec_datetime,
         * the subscription will be packed in this sp execution,
         * otherwise it has to wait for the next exec
         * (when the gate is open, you can update, cancel, or board
         * but when the gate cl0ses, you wait for the next flight)
         */
        savingsPlans {
            plan id 1001 of "US0378331005" run WEEKLY on 1
            plan id 1002 of "US0378331005" run WEEKLY on 6
            plan id 1003 of "US0378331005" run MONTHLY on 1
            plan id 1004 of "US0378331005" run MONTHLY on 15
        }

        subscriptions {
            subscribe sp 1001 by 10010 eur "10.00"
            subscribe sp 1002 by 10010 eur "10.00"
            subscribe sp 1003 by 10010 usd "10.00"
            subscribe sp 1004 by 10010 usd "10.00"
        }
    }

    println(" system savings plan - ${system.name}")
    println("#savings plans: ${system.savingsPlans}")
    println("#subscriptions: ${system.subscriptions}")
    println("#schdules: ${system.schedules}")

    val scheduler = PeriodicPollSavingsPlanScheduler()
    val dueSp = scheduler.nextSavingsPlan(system.schedules, system.savingsPlans)
    println("due sp: $dueSp")

}

package dsl

import java.util.Locale

// translation server side, domain model
data class Namespace(
    val namespace: String,
)

data class TranslationKey(
    val key: String,
)

data class Template(
    val template: String,
    val locale: Locale = Locale.ENGLISH,
) {
    private fun extractSubstrings(input: String): List<String> {
        val regex = "\\{\\{(.*?)}}".toRegex()
        return regex.findAll(input).map { it.groupValues[1] }.toList()
    }
    val parameters: List<String> = extractSubstrings(template)
}

// dictionary (namespace, key, locale) -> template

fun getTemplate(namespace: Namespace, key: TranslationKey, locale: Locale = Locale.ENGLISH): Template =
    if (locale == Locale.ENGLISH) {
        Template(
            template = "Hi, {{name}}! You have {{count}} new messages.",
            locale = locale,
        )
    } else {
        Template(
            template = "Hallo, {{name}}! Sie haben {{count}} neue Nachrichten.",
            locale = locale,
        )
    }

// locally on our server

data class TranslationAndCompileRequest(
    val defaultTemplate: Template,
    val parameters: Map<String, Any>,
    val locale: Locale,
    val translationKey: TranslationKey,
    val namespace: Namespace,
)

fun getDefaultTemplate(namespace: Namespace, key: TranslationKey): Template =
    Template(
        template = "Hi, {{name}}! You have {{count}} new messages.",
    )

fun requestForRemoteTemplate(
    namespace: Namespace,
    key: TranslationKey,
    locale: Locale,
): Template =
    getTemplate(namespace, key, locale)

fun compileTemplate(
    remoteTemplate: Template,
    parameters: Map<String, Any>,
): String {
    var compiledTemplate = remoteTemplate.template
    parameters.forEach { (key, value) ->
        compiledTemplate = compiledTemplate.replace("{{${key}}}", value.toString())
    }
    return compiledTemplate
}

// translation and compilation context: namespace, locale, parameters

interface Context {
    val namespace: Namespace
    val locale: Locale
    val parameters: Map<String, Any>
    val templates: Map<TranslationKey, Map<Locale, Template>>
}

object GlobalContext {
    val templates: Map<TranslationKey, Map<Locale, Template>> = mapOf(
        TranslationKey("welcome") to mapOf(
            Locale.ENGLISH to Template("Hi, {{name}}! You have {{count}} new messages."),
            Locale.GERMAN to Template("Hallo, {{name}}! Sie haben {{count}} neue Nachrichten."),
        )
    )

    val namespace = Namespace("default")
}

data class ContextImpl(
    val globalContext: GlobalContext,
    override val namespace: Namespace = globalContext.namespace,
    override val templates: Map<TranslationKey, Map<Locale, Template>> = globalContext.templates,
    override val locale: Locale,
    override val parameters: Map<String, Any>,
): Context

data class BodyTextRaw (
    val key: TranslationKey,
): Raw<BodyText> {
    override fun compile(ctx: Context): BodyText {
        BodyText(
            text = compileTemplate(
                remoteTemplate = ctx.templates[key]!![ctx.locale]!!,
                parameters = ctx.parameters,
            )
        )
    }
}

interface Raw<T> {
    fun compile(ctx: Context): T
}

// final components to compose, that we want in the end and the response send to the front end
interface Item
data class BodyText(
    val text: String
): Item

data class Icon(
    val url: String
): Item